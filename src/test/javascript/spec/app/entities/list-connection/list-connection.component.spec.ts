/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SamuScrummasterTestModule } from '../../../test.module';
import { ListConnectionComponent } from 'app/entities/list-connection/list-connection.component';
import { ListConnectionService } from 'app/entities/list-connection/list-connection.service';
import { ListConnection } from 'app/shared/model/list-connection.model';

describe('Component Tests', () => {
    describe('ListConnection Management Component', () => {
        let comp: ListConnectionComponent;
        let fixture: ComponentFixture<ListConnectionComponent>;
        let service: ListConnectionService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SamuScrummasterTestModule],
                declarations: [ListConnectionComponent],
                providers: []
            })
                .overrideTemplate(ListConnectionComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ListConnectionComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ListConnectionService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new ListConnection(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.listConnections[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
