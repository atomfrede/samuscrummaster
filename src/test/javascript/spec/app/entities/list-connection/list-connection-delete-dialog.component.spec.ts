/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SamuScrummasterTestModule } from '../../../test.module';
import { ListConnectionDeleteDialogComponent } from 'app/entities/list-connection/list-connection-delete-dialog.component';
import { ListConnectionService } from 'app/entities/list-connection/list-connection.service';

describe('Component Tests', () => {
    describe('ListConnection Management Delete Component', () => {
        let comp: ListConnectionDeleteDialogComponent;
        let fixture: ComponentFixture<ListConnectionDeleteDialogComponent>;
        let service: ListConnectionService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SamuScrummasterTestModule],
                declarations: [ListConnectionDeleteDialogComponent]
            })
                .overrideTemplate(ListConnectionDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ListConnectionDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ListConnectionService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
