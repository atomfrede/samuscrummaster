/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SamuScrummasterTestModule } from '../../../test.module';
import { ListConnectionDetailComponent } from 'app/entities/list-connection/list-connection-detail.component';
import { ListConnection } from 'app/shared/model/list-connection.model';

describe('Component Tests', () => {
    describe('ListConnection Management Detail Component', () => {
        let comp: ListConnectionDetailComponent;
        let fixture: ComponentFixture<ListConnectionDetailComponent>;
        const route = ({ data: of({ listConnection: new ListConnection(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SamuScrummasterTestModule],
                declarations: [ListConnectionDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(ListConnectionDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ListConnectionDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.listConnection).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
