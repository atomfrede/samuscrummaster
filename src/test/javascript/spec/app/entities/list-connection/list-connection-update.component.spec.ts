/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { SamuScrummasterTestModule } from '../../../test.module';
import { ListConnectionUpdateComponent } from 'app/entities/list-connection/list-connection-update.component';
import { ListConnectionService } from 'app/entities/list-connection/list-connection.service';
import { ListConnection } from 'app/shared/model/list-connection.model';

describe('Component Tests', () => {
    describe('ListConnection Management Update Component', () => {
        let comp: ListConnectionUpdateComponent;
        let fixture: ComponentFixture<ListConnectionUpdateComponent>;
        let service: ListConnectionService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SamuScrummasterTestModule],
                declarations: [ListConnectionUpdateComponent]
            })
                .overrideTemplate(ListConnectionUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(ListConnectionUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ListConnectionService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ListConnection(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.listConnection = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new ListConnection();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.listConnection = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
