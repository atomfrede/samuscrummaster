/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { SamuScrummasterTestModule } from '../../../test.module';
import { IssueListUpdateComponent } from 'app/entities/issue-list/issue-list-update.component';
import { IssueListService } from 'app/entities/issue-list/issue-list.service';
import { IssueList } from 'app/shared/model/issue-list.model';

describe('Component Tests', () => {
    describe('IssueList Management Update Component', () => {
        let comp: IssueListUpdateComponent;
        let fixture: ComponentFixture<IssueListUpdateComponent>;
        let service: IssueListService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SamuScrummasterTestModule],
                declarations: [IssueListUpdateComponent]
            })
                .overrideTemplate(IssueListUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(IssueListUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(IssueListService);
        });

        describe('save', () => {
            it(
                'Should call update service on save for existing entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new IssueList(123);
                    spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.issueList = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.update).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );

            it(
                'Should call create service on save for new entity',
                fakeAsync(() => {
                    // GIVEN
                    const entity = new IssueList();
                    spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                    comp.issueList = entity;
                    // WHEN
                    comp.save();
                    tick(); // simulate async

                    // THEN
                    expect(service.create).toHaveBeenCalledWith(entity);
                    expect(comp.isSaving).toEqual(false);
                })
            );
        });
    });
});
