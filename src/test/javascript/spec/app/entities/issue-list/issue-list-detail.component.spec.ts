/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SamuScrummasterTestModule } from '../../../test.module';
import { IssueListDetailComponent } from 'app/entities/issue-list/issue-list-detail.component';
import { IssueList } from 'app/shared/model/issue-list.model';

describe('Component Tests', () => {
    describe('IssueList Management Detail Component', () => {
        let comp: IssueListDetailComponent;
        let fixture: ComponentFixture<IssueListDetailComponent>;
        const route = ({ data: of({ issueList: new IssueList(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SamuScrummasterTestModule],
                declarations: [IssueListDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(IssueListDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(IssueListDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.issueList).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
