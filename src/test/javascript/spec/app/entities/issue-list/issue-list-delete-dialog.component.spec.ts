/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SamuScrummasterTestModule } from '../../../test.module';
import { IssueListDeleteDialogComponent } from 'app/entities/issue-list/issue-list-delete-dialog.component';
import { IssueListService } from 'app/entities/issue-list/issue-list.service';

describe('Component Tests', () => {
    describe('IssueList Management Delete Component', () => {
        let comp: IssueListDeleteDialogComponent;
        let fixture: ComponentFixture<IssueListDeleteDialogComponent>;
        let service: IssueListService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SamuScrummasterTestModule],
                declarations: [IssueListDeleteDialogComponent]
            })
                .overrideTemplate(IssueListDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(IssueListDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(IssueListService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it(
                'Should call delete service on confirmDelete',
                inject(
                    [],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });
});
