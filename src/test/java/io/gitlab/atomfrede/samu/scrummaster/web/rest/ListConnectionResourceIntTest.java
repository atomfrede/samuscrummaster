package io.gitlab.atomfrede.samu.scrummaster.web.rest;

import io.gitlab.atomfrede.samu.scrummaster.SamuScrummasterApp;

import io.gitlab.atomfrede.samu.scrummaster.domain.ListConnection;
import io.gitlab.atomfrede.samu.scrummaster.repository.ListConnectionRepository;
import io.gitlab.atomfrede.samu.scrummaster.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static io.gitlab.atomfrede.samu.scrummaster.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import io.gitlab.atomfrede.samu.scrummaster.domain.enumeration.ListRole;
/**
 * Test class for the ListConnectionResource REST controller.
 *
 * @see ListConnectionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SamuScrummasterApp.class)
public class ListConnectionResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final ListRole DEFAULT_TYPE = ListRole.INBOX;
    private static final ListRole UPDATED_TYPE = ListRole.OUTBOX;

    @Autowired
    private ListConnectionRepository listConnectionRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restListConnectionMockMvc;

    private ListConnection listConnection;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ListConnectionResource listConnectionResource = new ListConnectionResource(listConnectionRepository);
        this.restListConnectionMockMvc = MockMvcBuilders.standaloneSetup(listConnectionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ListConnection createEntity(EntityManager em) {
        ListConnection listConnection = new ListConnection()
            .name(DEFAULT_NAME)
            .type(DEFAULT_TYPE);
        return listConnection;
    }

    @Before
    public void initTest() {
        listConnection = createEntity(em);
    }

    @Test
    @Transactional
    public void createListConnection() throws Exception {
        int databaseSizeBeforeCreate = listConnectionRepository.findAll().size();

        // Create the ListConnection
        restListConnectionMockMvc.perform(post("/api/list-connections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(listConnection)))
            .andExpect(status().isCreated());

        // Validate the ListConnection in the database
        List<ListConnection> listConnectionList = listConnectionRepository.findAll();
        assertThat(listConnectionList).hasSize(databaseSizeBeforeCreate + 1);
        ListConnection testListConnection = listConnectionList.get(listConnectionList.size() - 1);
        assertThat(testListConnection.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testListConnection.getType()).isEqualTo(DEFAULT_TYPE);
    }

    @Test
    @Transactional
    public void createListConnectionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = listConnectionRepository.findAll().size();

        // Create the ListConnection with an existing ID
        listConnection.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restListConnectionMockMvc.perform(post("/api/list-connections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(listConnection)))
            .andExpect(status().isBadRequest());

        // Validate the ListConnection in the database
        List<ListConnection> listConnectionList = listConnectionRepository.findAll();
        assertThat(listConnectionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = listConnectionRepository.findAll().size();
        // set the field null
        listConnection.setName(null);

        // Create the ListConnection, which fails.

        restListConnectionMockMvc.perform(post("/api/list-connections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(listConnection)))
            .andExpect(status().isBadRequest());

        List<ListConnection> listConnectionList = listConnectionRepository.findAll();
        assertThat(listConnectionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = listConnectionRepository.findAll().size();
        // set the field null
        listConnection.setType(null);

        // Create the ListConnection, which fails.

        restListConnectionMockMvc.perform(post("/api/list-connections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(listConnection)))
            .andExpect(status().isBadRequest());

        List<ListConnection> listConnectionList = listConnectionRepository.findAll();
        assertThat(listConnectionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllListConnections() throws Exception {
        // Initialize the database
        listConnectionRepository.saveAndFlush(listConnection);

        // Get all the listConnectionList
        restListConnectionMockMvc.perform(get("/api/list-connections?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(listConnection.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }
    
    @Test
    @Transactional
    public void getListConnection() throws Exception {
        // Initialize the database
        listConnectionRepository.saveAndFlush(listConnection);

        // Get the listConnection
        restListConnectionMockMvc.perform(get("/api/list-connections/{id}", listConnection.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(listConnection.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingListConnection() throws Exception {
        // Get the listConnection
        restListConnectionMockMvc.perform(get("/api/list-connections/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateListConnection() throws Exception {
        // Initialize the database
        listConnectionRepository.saveAndFlush(listConnection);

        int databaseSizeBeforeUpdate = listConnectionRepository.findAll().size();

        // Update the listConnection
        ListConnection updatedListConnection = listConnectionRepository.findById(listConnection.getId()).get();
        // Disconnect from session so that the updates on updatedListConnection are not directly saved in db
        em.detach(updatedListConnection);
        updatedListConnection
            .name(UPDATED_NAME)
            .type(UPDATED_TYPE);

        restListConnectionMockMvc.perform(put("/api/list-connections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedListConnection)))
            .andExpect(status().isOk());

        // Validate the ListConnection in the database
        List<ListConnection> listConnectionList = listConnectionRepository.findAll();
        assertThat(listConnectionList).hasSize(databaseSizeBeforeUpdate);
        ListConnection testListConnection = listConnectionList.get(listConnectionList.size() - 1);
        assertThat(testListConnection.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testListConnection.getType()).isEqualTo(UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingListConnection() throws Exception {
        int databaseSizeBeforeUpdate = listConnectionRepository.findAll().size();

        // Create the ListConnection

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restListConnectionMockMvc.perform(put("/api/list-connections")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(listConnection)))
            .andExpect(status().isBadRequest());

        // Validate the ListConnection in the database
        List<ListConnection> listConnectionList = listConnectionRepository.findAll();
        assertThat(listConnectionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteListConnection() throws Exception {
        // Initialize the database
        listConnectionRepository.saveAndFlush(listConnection);

        int databaseSizeBeforeDelete = listConnectionRepository.findAll().size();

        // Get the listConnection
        restListConnectionMockMvc.perform(delete("/api/list-connections/{id}", listConnection.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ListConnection> listConnectionList = listConnectionRepository.findAll();
        assertThat(listConnectionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ListConnection.class);
        ListConnection listConnection1 = new ListConnection();
        listConnection1.setId(1L);
        ListConnection listConnection2 = new ListConnection();
        listConnection2.setId(listConnection1.getId());
        assertThat(listConnection1).isEqualTo(listConnection2);
        listConnection2.setId(2L);
        assertThat(listConnection1).isNotEqualTo(listConnection2);
        listConnection1.setId(null);
        assertThat(listConnection1).isNotEqualTo(listConnection2);
    }
}
