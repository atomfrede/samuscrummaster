package io.gitlab.atomfrede.samu.scrummaster.web.rest;

import io.gitlab.atomfrede.samu.scrummaster.SamuScrummasterApp;

import io.gitlab.atomfrede.samu.scrummaster.domain.IssueList;
import io.gitlab.atomfrede.samu.scrummaster.repository.IssueListRepository;
import io.gitlab.atomfrede.samu.scrummaster.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static io.gitlab.atomfrede.samu.scrummaster.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the IssueListResource REST controller.
 *
 * @see IssueListResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SamuScrummasterApp.class)
public class IssueListResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private IssueListRepository issueListRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restIssueListMockMvc;

    private IssueList issueList;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final IssueListResource issueListResource = new IssueListResource(issueListRepository);
        this.restIssueListMockMvc = MockMvcBuilders.standaloneSetup(issueListResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static IssueList createEntity(EntityManager em) {
        IssueList issueList = new IssueList()
            .name(DEFAULT_NAME);
        return issueList;
    }

    @Before
    public void initTest() {
        issueList = createEntity(em);
    }

    @Test
    @Transactional
    public void createIssueList() throws Exception {
        int databaseSizeBeforeCreate = issueListRepository.findAll().size();

        // Create the IssueList
        restIssueListMockMvc.perform(post("/api/issue-lists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(issueList)))
            .andExpect(status().isCreated());

        // Validate the IssueList in the database
        List<IssueList> issueListList = issueListRepository.findAll();
        assertThat(issueListList).hasSize(databaseSizeBeforeCreate + 1);
        IssueList testIssueList = issueListList.get(issueListList.size() - 1);
        assertThat(testIssueList.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createIssueListWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = issueListRepository.findAll().size();

        // Create the IssueList with an existing ID
        issueList.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restIssueListMockMvc.perform(post("/api/issue-lists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(issueList)))
            .andExpect(status().isBadRequest());

        // Validate the IssueList in the database
        List<IssueList> issueListList = issueListRepository.findAll();
        assertThat(issueListList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = issueListRepository.findAll().size();
        // set the field null
        issueList.setName(null);

        // Create the IssueList, which fails.

        restIssueListMockMvc.perform(post("/api/issue-lists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(issueList)))
            .andExpect(status().isBadRequest());

        List<IssueList> issueListList = issueListRepository.findAll();
        assertThat(issueListList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllIssueLists() throws Exception {
        // Initialize the database
        issueListRepository.saveAndFlush(issueList);

        // Get all the issueListList
        restIssueListMockMvc.perform(get("/api/issue-lists?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(issueList.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }
    
    @Test
    @Transactional
    public void getIssueList() throws Exception {
        // Initialize the database
        issueListRepository.saveAndFlush(issueList);

        // Get the issueList
        restIssueListMockMvc.perform(get("/api/issue-lists/{id}", issueList.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(issueList.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingIssueList() throws Exception {
        // Get the issueList
        restIssueListMockMvc.perform(get("/api/issue-lists/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateIssueList() throws Exception {
        // Initialize the database
        issueListRepository.saveAndFlush(issueList);

        int databaseSizeBeforeUpdate = issueListRepository.findAll().size();

        // Update the issueList
        IssueList updatedIssueList = issueListRepository.findById(issueList.getId()).get();
        // Disconnect from session so that the updates on updatedIssueList are not directly saved in db
        em.detach(updatedIssueList);
        updatedIssueList
            .name(UPDATED_NAME);

        restIssueListMockMvc.perform(put("/api/issue-lists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedIssueList)))
            .andExpect(status().isOk());

        // Validate the IssueList in the database
        List<IssueList> issueListList = issueListRepository.findAll();
        assertThat(issueListList).hasSize(databaseSizeBeforeUpdate);
        IssueList testIssueList = issueListList.get(issueListList.size() - 1);
        assertThat(testIssueList.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingIssueList() throws Exception {
        int databaseSizeBeforeUpdate = issueListRepository.findAll().size();

        // Create the IssueList

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIssueListMockMvc.perform(put("/api/issue-lists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(issueList)))
            .andExpect(status().isBadRequest());

        // Validate the IssueList in the database
        List<IssueList> issueListList = issueListRepository.findAll();
        assertThat(issueListList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteIssueList() throws Exception {
        // Initialize the database
        issueListRepository.saveAndFlush(issueList);

        int databaseSizeBeforeDelete = issueListRepository.findAll().size();

        // Get the issueList
        restIssueListMockMvc.perform(delete("/api/issue-lists/{id}", issueList.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<IssueList> issueListList = issueListRepository.findAll();
        assertThat(issueListList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(IssueList.class);
        IssueList issueList1 = new IssueList();
        issueList1.setId(1L);
        IssueList issueList2 = new IssueList();
        issueList2.setId(issueList1.getId());
        assertThat(issueList1).isEqualTo(issueList2);
        issueList2.setId(2L);
        assertThat(issueList1).isNotEqualTo(issueList2);
        issueList1.setId(null);
        assertThat(issueList1).isNotEqualTo(issueList2);
    }
}
