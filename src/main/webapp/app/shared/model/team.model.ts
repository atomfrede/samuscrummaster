import { IWorkspace } from 'app/shared/model//workspace.model';

export interface ITeam {
    id?: number;
    name?: string;
    workspace?: IWorkspace;
}

export class Team implements ITeam {
    constructor(public id?: number, public name?: string, public workspace?: IWorkspace) {}
}
