import { IIssue } from 'app/shared/model//issue.model';
import { IListConnection } from 'app/shared/model//list-connection.model';

export interface IIssueList {
    id?: number;
    name?: string;
    issues?: IIssue[];
    listConnections?: IListConnection[];
}

export class IssueList implements IIssueList {
    constructor(public id?: number, public name?: string, public issues?: IIssue[], public listConnections?: IListConnection[]) {}
}
