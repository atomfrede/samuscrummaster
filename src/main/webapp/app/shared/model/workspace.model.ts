import { IListConnection } from 'app/shared/model//list-connection.model';
import { ITeam } from 'app/shared/model//team.model';

export interface IWorkspace {
    id?: number;
    name?: string;
    listConnections?: IListConnection[];
    team?: ITeam;
}

export class Workspace implements IWorkspace {
    constructor(public id?: number, public name?: string, public listConnections?: IListConnection[], public team?: ITeam) {}
}
