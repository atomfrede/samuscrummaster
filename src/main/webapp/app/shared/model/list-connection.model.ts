import { IWorkspace } from 'app/shared/model//workspace.model';
import { IIssueList } from 'app/shared/model//issue-list.model';

export const enum ListRole {
    INBOX = 'INBOX',
    OUTBOX = 'OUTBOX',
    INTERNAL = 'INTERNAL'
}

export interface IListConnection {
    id?: number;
    name?: string;
    type?: ListRole;
    workspace?: IWorkspace;
    issueList?: IIssueList;
}

export class ListConnection implements IListConnection {
    constructor(
        public id?: number,
        public name?: string,
        public type?: ListRole,
        public workspace?: IWorkspace,
        public issueList?: IIssueList
    ) {}
}
