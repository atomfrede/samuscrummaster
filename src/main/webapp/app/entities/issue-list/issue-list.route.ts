import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { IssueList } from 'app/shared/model/issue-list.model';
import { IssueListService } from './issue-list.service';
import { IssueListComponent } from './issue-list.component';
import { IssueListDetailComponent } from './issue-list-detail.component';
import { IssueListUpdateComponent } from './issue-list-update.component';
import { IssueListDeletePopupComponent } from './issue-list-delete-dialog.component';
import { IIssueList } from 'app/shared/model/issue-list.model';

@Injectable({ providedIn: 'root' })
export class IssueListResolve implements Resolve<IIssueList> {
    constructor(private service: IssueListService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((issueList: HttpResponse<IssueList>) => issueList.body));
        }
        return of(new IssueList());
    }
}

export const issueListRoute: Routes = [
    {
        path: 'issue-list',
        component: IssueListComponent,
        resolve: {
            pagingParams: JhiResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            defaultSort: 'id,asc',
            pageTitle: 'samuScrummasterApp.issueList.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'issue-list/:id/view',
        component: IssueListDetailComponent,
        resolve: {
            issueList: IssueListResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'samuScrummasterApp.issueList.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'issue-list/new',
        component: IssueListUpdateComponent,
        resolve: {
            issueList: IssueListResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'samuScrummasterApp.issueList.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'issue-list/:id/edit',
        component: IssueListUpdateComponent,
        resolve: {
            issueList: IssueListResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'samuScrummasterApp.issueList.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const issueListPopupRoute: Routes = [
    {
        path: 'issue-list/:id/delete',
        component: IssueListDeletePopupComponent,
        resolve: {
            issueList: IssueListResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'samuScrummasterApp.issueList.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
