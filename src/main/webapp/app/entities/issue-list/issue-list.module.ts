import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SamuScrummasterSharedModule } from 'app/shared';
import {
    IssueListComponent,
    IssueListDetailComponent,
    IssueListUpdateComponent,
    IssueListDeletePopupComponent,
    IssueListDeleteDialogComponent,
    issueListRoute,
    issueListPopupRoute
} from './';

const ENTITY_STATES = [...issueListRoute, ...issueListPopupRoute];

@NgModule({
    imports: [SamuScrummasterSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        IssueListComponent,
        IssueListDetailComponent,
        IssueListUpdateComponent,
        IssueListDeleteDialogComponent,
        IssueListDeletePopupComponent
    ],
    entryComponents: [IssueListComponent, IssueListUpdateComponent, IssueListDeleteDialogComponent, IssueListDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SamuScrummasterIssueListModule {}
