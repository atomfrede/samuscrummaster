import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IIssueList } from 'app/shared/model/issue-list.model';

@Component({
    selector: 'jhi-issue-list-detail',
    templateUrl: './issue-list-detail.component.html'
})
export class IssueListDetailComponent implements OnInit {
    issueList: IIssueList;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ issueList }) => {
            this.issueList = issueList;
        });
    }

    previousState() {
        window.history.back();
    }
}
