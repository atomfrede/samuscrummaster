import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IIssueList } from 'app/shared/model/issue-list.model';
import { IssueListService } from './issue-list.service';

@Component({
    selector: 'jhi-issue-list-update',
    templateUrl: './issue-list-update.component.html'
})
export class IssueListUpdateComponent implements OnInit {
    issueList: IIssueList;
    isSaving: boolean;

    constructor(private issueListService: IssueListService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ issueList }) => {
            this.issueList = issueList;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.issueList.id !== undefined) {
            this.subscribeToSaveResponse(this.issueListService.update(this.issueList));
        } else {
            this.subscribeToSaveResponse(this.issueListService.create(this.issueList));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IIssueList>>) {
        result.subscribe((res: HttpResponse<IIssueList>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
}
