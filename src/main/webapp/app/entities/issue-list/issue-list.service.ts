import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IIssueList } from 'app/shared/model/issue-list.model';

type EntityResponseType = HttpResponse<IIssueList>;
type EntityArrayResponseType = HttpResponse<IIssueList[]>;

@Injectable({ providedIn: 'root' })
export class IssueListService {
    public resourceUrl = SERVER_API_URL + 'api/issue-lists';

    constructor(private http: HttpClient) {}

    create(issueList: IIssueList): Observable<EntityResponseType> {
        return this.http.post<IIssueList>(this.resourceUrl, issueList, { observe: 'response' });
    }

    update(issueList: IIssueList): Observable<EntityResponseType> {
        return this.http.put<IIssueList>(this.resourceUrl, issueList, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IIssueList>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IIssueList[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
