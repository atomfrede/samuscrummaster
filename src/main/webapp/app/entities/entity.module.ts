import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { SamuScrummasterTeamModule } from './team/team.module';
import { SamuScrummasterWorkspaceModule } from './workspace/workspace.module';
import { SamuScrummasterIssueListModule } from './issue-list/issue-list.module';
import { SamuScrummasterIssueModule } from './issue/issue.module';
import { SamuScrummasterListConnectionModule } from './list-connection/list-connection.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        SamuScrummasterTeamModule,
        SamuScrummasterWorkspaceModule,
        SamuScrummasterIssueListModule,
        SamuScrummasterIssueModule,
        SamuScrummasterListConnectionModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SamuScrummasterEntityModule {}
