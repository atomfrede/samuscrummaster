import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IWorkspace } from 'app/shared/model/workspace.model';
import { WorkspaceService } from './workspace.service';
import { ITeam } from 'app/shared/model/team.model';
import { TeamService } from 'app/entities/team';

@Component({
    selector: 'jhi-workspace-update',
    templateUrl: './workspace-update.component.html'
})
export class WorkspaceUpdateComponent implements OnInit {
    workspace: IWorkspace;
    isSaving: boolean;

    teams: ITeam[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private workspaceService: WorkspaceService,
        private teamService: TeamService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ workspace }) => {
            this.workspace = workspace;
        });
        this.teamService.query().subscribe(
            (res: HttpResponse<ITeam[]>) => {
                this.teams = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.workspace.id !== undefined) {
            this.subscribeToSaveResponse(this.workspaceService.update(this.workspace));
        } else {
            this.subscribeToSaveResponse(this.workspaceService.create(this.workspace));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IWorkspace>>) {
        result.subscribe((res: HttpResponse<IWorkspace>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackTeamById(index: number, item: ITeam) {
        return item.id;
    }
}
