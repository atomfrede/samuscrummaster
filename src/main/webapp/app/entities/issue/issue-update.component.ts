import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IIssue } from 'app/shared/model/issue.model';
import { IssueService } from './issue.service';
import { IIssueList } from 'app/shared/model/issue-list.model';
import { IssueListService } from 'app/entities/issue-list';

@Component({
    selector: 'jhi-issue-update',
    templateUrl: './issue-update.component.html'
})
export class IssueUpdateComponent implements OnInit {
    issue: IIssue;
    isSaving: boolean;

    issuelists: IIssueList[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private issueService: IssueService,
        private issueListService: IssueListService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ issue }) => {
            this.issue = issue;
        });
        this.issueListService.query().subscribe(
            (res: HttpResponse<IIssueList[]>) => {
                this.issuelists = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.issue.id !== undefined) {
            this.subscribeToSaveResponse(this.issueService.update(this.issue));
        } else {
            this.subscribeToSaveResponse(this.issueService.create(this.issue));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IIssue>>) {
        result.subscribe((res: HttpResponse<IIssue>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackIssueListById(index: number, item: IIssueList) {
        return item.id;
    }
}
