import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IListConnection } from 'app/shared/model/list-connection.model';
import { ListConnectionService } from './list-connection.service';
import { IWorkspace } from 'app/shared/model/workspace.model';
import { WorkspaceService } from 'app/entities/workspace';
import { IIssueList } from 'app/shared/model/issue-list.model';
import { IssueListService } from 'app/entities/issue-list';

@Component({
    selector: 'jhi-list-connection-update',
    templateUrl: './list-connection-update.component.html'
})
export class ListConnectionUpdateComponent implements OnInit {
    listConnection: IListConnection;
    isSaving: boolean;

    workspaces: IWorkspace[];

    issuelists: IIssueList[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private listConnectionService: ListConnectionService,
        private workspaceService: WorkspaceService,
        private issueListService: IssueListService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ listConnection }) => {
            this.listConnection = listConnection;
        });
        this.workspaceService.query().subscribe(
            (res: HttpResponse<IWorkspace[]>) => {
                this.workspaces = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.issueListService.query().subscribe(
            (res: HttpResponse<IIssueList[]>) => {
                this.issuelists = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.listConnection.id !== undefined) {
            this.subscribeToSaveResponse(this.listConnectionService.update(this.listConnection));
        } else {
            this.subscribeToSaveResponse(this.listConnectionService.create(this.listConnection));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IListConnection>>) {
        result.subscribe((res: HttpResponse<IListConnection>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackWorkspaceById(index: number, item: IWorkspace) {
        return item.id;
    }

    trackIssueListById(index: number, item: IIssueList) {
        return item.id;
    }
}
