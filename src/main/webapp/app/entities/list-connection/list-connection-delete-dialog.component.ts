import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IListConnection } from 'app/shared/model/list-connection.model';
import { ListConnectionService } from './list-connection.service';

@Component({
    selector: 'jhi-list-connection-delete-dialog',
    templateUrl: './list-connection-delete-dialog.component.html'
})
export class ListConnectionDeleteDialogComponent {
    listConnection: IListConnection;

    constructor(
        private listConnectionService: ListConnectionService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.listConnectionService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'listConnectionListModification',
                content: 'Deleted an listConnection'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-list-connection-delete-popup',
    template: ''
})
export class ListConnectionDeletePopupComponent implements OnInit, OnDestroy {
    private ngbModalRef: NgbModalRef;

    constructor(private activatedRoute: ActivatedRoute, private router: Router, private modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ listConnection }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ListConnectionDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.listConnection = listConnection;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate([{ outlets: { popup: null } }], { replaceUrl: true, queryParamsHandling: 'merge' });
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
