import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IListConnection } from 'app/shared/model/list-connection.model';
import { Principal } from 'app/core';
import { ListConnectionService } from './list-connection.service';

@Component({
    selector: 'jhi-list-connection',
    templateUrl: './list-connection.component.html'
})
export class ListConnectionComponent implements OnInit, OnDestroy {
    listConnections: IListConnection[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private listConnectionService: ListConnectionService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.listConnectionService.query().subscribe(
            (res: HttpResponse<IListConnection[]>) => {
                this.listConnections = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInListConnections();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IListConnection) {
        return item.id;
    }

    registerChangeInListConnections() {
        this.eventSubscriber = this.eventManager.subscribe('listConnectionListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
