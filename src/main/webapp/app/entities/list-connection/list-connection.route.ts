import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ListConnection } from 'app/shared/model/list-connection.model';
import { ListConnectionService } from './list-connection.service';
import { ListConnectionComponent } from './list-connection.component';
import { ListConnectionDetailComponent } from './list-connection-detail.component';
import { ListConnectionUpdateComponent } from './list-connection-update.component';
import { ListConnectionDeletePopupComponent } from './list-connection-delete-dialog.component';
import { IListConnection } from 'app/shared/model/list-connection.model';

@Injectable({ providedIn: 'root' })
export class ListConnectionResolve implements Resolve<IListConnection> {
    constructor(private service: ListConnectionService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(map((listConnection: HttpResponse<ListConnection>) => listConnection.body));
        }
        return of(new ListConnection());
    }
}

export const listConnectionRoute: Routes = [
    {
        path: 'list-connection',
        component: ListConnectionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'samuScrummasterApp.listConnection.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'list-connection/:id/view',
        component: ListConnectionDetailComponent,
        resolve: {
            listConnection: ListConnectionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'samuScrummasterApp.listConnection.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'list-connection/new',
        component: ListConnectionUpdateComponent,
        resolve: {
            listConnection: ListConnectionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'samuScrummasterApp.listConnection.home.title'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'list-connection/:id/edit',
        component: ListConnectionUpdateComponent,
        resolve: {
            listConnection: ListConnectionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'samuScrummasterApp.listConnection.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const listConnectionPopupRoute: Routes = [
    {
        path: 'list-connection/:id/delete',
        component: ListConnectionDeletePopupComponent,
        resolve: {
            listConnection: ListConnectionResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'samuScrummasterApp.listConnection.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
