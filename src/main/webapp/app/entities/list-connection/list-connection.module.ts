import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SamuScrummasterSharedModule } from 'app/shared';
import {
    ListConnectionComponent,
    ListConnectionDetailComponent,
    ListConnectionUpdateComponent,
    ListConnectionDeletePopupComponent,
    ListConnectionDeleteDialogComponent,
    listConnectionRoute,
    listConnectionPopupRoute
} from './';

const ENTITY_STATES = [...listConnectionRoute, ...listConnectionPopupRoute];

@NgModule({
    imports: [SamuScrummasterSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ListConnectionComponent,
        ListConnectionDetailComponent,
        ListConnectionUpdateComponent,
        ListConnectionDeleteDialogComponent,
        ListConnectionDeletePopupComponent
    ],
    entryComponents: [
        ListConnectionComponent,
        ListConnectionUpdateComponent,
        ListConnectionDeleteDialogComponent,
        ListConnectionDeletePopupComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SamuScrummasterListConnectionModule {}
