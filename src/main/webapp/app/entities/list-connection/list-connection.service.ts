import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IListConnection } from 'app/shared/model/list-connection.model';

type EntityResponseType = HttpResponse<IListConnection>;
type EntityArrayResponseType = HttpResponse<IListConnection[]>;

@Injectable({ providedIn: 'root' })
export class ListConnectionService {
    public resourceUrl = SERVER_API_URL + 'api/list-connections';

    constructor(private http: HttpClient) {}

    create(listConnection: IListConnection): Observable<EntityResponseType> {
        return this.http.post<IListConnection>(this.resourceUrl, listConnection, { observe: 'response' });
    }

    update(listConnection: IListConnection): Observable<EntityResponseType> {
        return this.http.put<IListConnection>(this.resourceUrl, listConnection, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IListConnection>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IListConnection[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
