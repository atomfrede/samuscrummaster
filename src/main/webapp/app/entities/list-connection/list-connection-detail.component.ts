import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IListConnection } from 'app/shared/model/list-connection.model';

@Component({
    selector: 'jhi-list-connection-detail',
    templateUrl: './list-connection-detail.component.html'
})
export class ListConnectionDetailComponent implements OnInit {
    listConnection: IListConnection;

    constructor(private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ listConnection }) => {
            this.listConnection = listConnection;
        });
    }

    previousState() {
        window.history.back();
    }
}
