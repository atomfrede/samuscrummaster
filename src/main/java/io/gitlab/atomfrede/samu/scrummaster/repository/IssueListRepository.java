package io.gitlab.atomfrede.samu.scrummaster.repository;

import io.gitlab.atomfrede.samu.scrummaster.domain.IssueList;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the IssueList entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IssueListRepository extends JpaRepository<IssueList, Long> {

}
