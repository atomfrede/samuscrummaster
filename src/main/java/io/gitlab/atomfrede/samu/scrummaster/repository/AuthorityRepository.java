package io.gitlab.atomfrede.samu.scrummaster.repository;

import io.gitlab.atomfrede.samu.scrummaster.domain.Authority;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
