package io.gitlab.atomfrede.samu.scrummaster.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import io.github.jhipster.config.jcache.BeanClassLoaderAwareJCacheRegionFactory;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        BeanClassLoaderAwareJCacheRegionFactory.setBeanClassLoader(this.getClass().getClassLoader());
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(io.gitlab.atomfrede.samu.scrummaster.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(io.gitlab.atomfrede.samu.scrummaster.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(io.gitlab.atomfrede.samu.scrummaster.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(io.gitlab.atomfrede.samu.scrummaster.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(io.gitlab.atomfrede.samu.scrummaster.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(io.gitlab.atomfrede.samu.scrummaster.domain.Team.class.getName(), jcacheConfiguration);
            cm.createCache(io.gitlab.atomfrede.samu.scrummaster.domain.Workspace.class.getName(), jcacheConfiguration);
            cm.createCache(io.gitlab.atomfrede.samu.scrummaster.domain.Workspace.class.getName() + ".listConnections", jcacheConfiguration);
            cm.createCache(io.gitlab.atomfrede.samu.scrummaster.domain.IssueList.class.getName(), jcacheConfiguration);
            cm.createCache(io.gitlab.atomfrede.samu.scrummaster.domain.IssueList.class.getName() + ".issues", jcacheConfiguration);
            cm.createCache(io.gitlab.atomfrede.samu.scrummaster.domain.IssueList.class.getName() + ".listConnections", jcacheConfiguration);
            cm.createCache(io.gitlab.atomfrede.samu.scrummaster.domain.Issue.class.getName(), jcacheConfiguration);
            cm.createCache(io.gitlab.atomfrede.samu.scrummaster.domain.ListConnection.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
