/**
 * Spring MVC REST controllers.
 */
package io.gitlab.atomfrede.samu.scrummaster.web.rest;
