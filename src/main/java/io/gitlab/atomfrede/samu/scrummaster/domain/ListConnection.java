package io.gitlab.atomfrede.samu.scrummaster.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

import io.gitlab.atomfrede.samu.scrummaster.domain.enumeration.ListRole;

/**
 * A ListConnection.
 */
@Entity
@Table(name = "list_connection")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ListConnection implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_type", nullable = false)
    private ListRole type;

    @ManyToOne
    @JsonIgnoreProperties("listConnections")
    private Workspace workspace;

    @ManyToOne
    @JsonIgnoreProperties("listConnections")
    private IssueList issueList;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public ListConnection name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ListRole getType() {
        return type;
    }

    public ListConnection type(ListRole type) {
        this.type = type;
        return this;
    }

    public void setType(ListRole type) {
        this.type = type;
    }

    public Workspace getWorkspace() {
        return workspace;
    }

    public ListConnection workspace(Workspace workspace) {
        this.workspace = workspace;
        return this;
    }

    public void setWorkspace(Workspace workspace) {
        this.workspace = workspace;
    }

    public IssueList getIssueList() {
        return issueList;
    }

    public ListConnection issueList(IssueList issueList) {
        this.issueList = issueList;
        return this;
    }

    public void setIssueList(IssueList issueList) {
        this.issueList = issueList;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ListConnection listConnection = (ListConnection) o;
        if (listConnection.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), listConnection.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ListConnection{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
