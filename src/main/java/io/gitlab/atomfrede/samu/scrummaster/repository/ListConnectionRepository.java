package io.gitlab.atomfrede.samu.scrummaster.repository;

import io.gitlab.atomfrede.samu.scrummaster.domain.ListConnection;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ListConnection entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ListConnectionRepository extends JpaRepository<ListConnection, Long> {

}
