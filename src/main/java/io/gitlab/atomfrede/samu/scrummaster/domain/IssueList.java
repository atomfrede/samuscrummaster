package io.gitlab.atomfrede.samu.scrummaster.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A IssueList.
 */
@Entity
@Table(name = "issue_list")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class IssueList implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "issueList")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Issue> issues = new HashSet<>();
    @OneToMany(mappedBy = "issueList")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<ListConnection> listConnections = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public IssueList name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Issue> getIssues() {
        return issues;
    }

    public IssueList issues(Set<Issue> issues) {
        this.issues = issues;
        return this;
    }

    public IssueList addIssue(Issue issue) {
        this.issues.add(issue);
        issue.setIssueList(this);
        return this;
    }

    public IssueList removeIssue(Issue issue) {
        this.issues.remove(issue);
        issue.setIssueList(null);
        return this;
    }

    public void setIssues(Set<Issue> issues) {
        this.issues = issues;
    }

    public Set<ListConnection> getListConnections() {
        return listConnections;
    }

    public IssueList listConnections(Set<ListConnection> listConnections) {
        this.listConnections = listConnections;
        return this;
    }

    public IssueList addListConnection(ListConnection listConnection) {
        this.listConnections.add(listConnection);
        listConnection.setIssueList(this);
        return this;
    }

    public IssueList removeListConnection(ListConnection listConnection) {
        this.listConnections.remove(listConnection);
        listConnection.setIssueList(null);
        return this;
    }

    public void setListConnections(Set<ListConnection> listConnections) {
        this.listConnections = listConnections;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        IssueList issueList = (IssueList) o;
        if (issueList.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), issueList.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "IssueList{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
