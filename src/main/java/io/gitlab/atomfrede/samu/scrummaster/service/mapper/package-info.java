/**
 * MapStruct mappers for mapping domain objects and Data Transfer Objects.
 */
package io.gitlab.atomfrede.samu.scrummaster.service.mapper;
