package io.gitlab.atomfrede.samu.scrummaster.domain.enumeration;

/**
 * The ListRole enumeration.
 */
public enum ListRole {
    INBOX, OUTBOX, INTERNAL
}
