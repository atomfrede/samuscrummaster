package io.gitlab.atomfrede.samu.scrummaster.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.gitlab.atomfrede.samu.scrummaster.domain.ListConnection;
import io.gitlab.atomfrede.samu.scrummaster.repository.ListConnectionRepository;
import io.gitlab.atomfrede.samu.scrummaster.web.rest.errors.BadRequestAlertException;
import io.gitlab.atomfrede.samu.scrummaster.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing ListConnection.
 */
@RestController
@RequestMapping("/api")
public class ListConnectionResource {

    private final Logger log = LoggerFactory.getLogger(ListConnectionResource.class);

    private static final String ENTITY_NAME = "listConnection";

    private ListConnectionRepository listConnectionRepository;

    public ListConnectionResource(ListConnectionRepository listConnectionRepository) {
        this.listConnectionRepository = listConnectionRepository;
    }

    /**
     * POST  /list-connections : Create a new listConnection.
     *
     * @param listConnection the listConnection to create
     * @return the ResponseEntity with status 201 (Created) and with body the new listConnection, or with status 400 (Bad Request) if the listConnection has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/list-connections")
    @Timed
    public ResponseEntity<ListConnection> createListConnection(@Valid @RequestBody ListConnection listConnection) throws URISyntaxException {
        log.debug("REST request to save ListConnection : {}", listConnection);
        if (listConnection.getId() != null) {
            throw new BadRequestAlertException("A new listConnection cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ListConnection result = listConnectionRepository.save(listConnection);
        return ResponseEntity.created(new URI("/api/list-connections/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /list-connections : Updates an existing listConnection.
     *
     * @param listConnection the listConnection to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated listConnection,
     * or with status 400 (Bad Request) if the listConnection is not valid,
     * or with status 500 (Internal Server Error) if the listConnection couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/list-connections")
    @Timed
    public ResponseEntity<ListConnection> updateListConnection(@Valid @RequestBody ListConnection listConnection) throws URISyntaxException {
        log.debug("REST request to update ListConnection : {}", listConnection);
        if (listConnection.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ListConnection result = listConnectionRepository.save(listConnection);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, listConnection.getId().toString()))
            .body(result);
    }

    /**
     * GET  /list-connections : get all the listConnections.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of listConnections in body
     */
    @GetMapping("/list-connections")
    @Timed
    public List<ListConnection> getAllListConnections() {
        log.debug("REST request to get all ListConnections");
        return listConnectionRepository.findAll();
    }

    /**
     * GET  /list-connections/:id : get the "id" listConnection.
     *
     * @param id the id of the listConnection to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the listConnection, or with status 404 (Not Found)
     */
    @GetMapping("/list-connections/{id}")
    @Timed
    public ResponseEntity<ListConnection> getListConnection(@PathVariable Long id) {
        log.debug("REST request to get ListConnection : {}", id);
        Optional<ListConnection> listConnection = listConnectionRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(listConnection);
    }

    /**
     * DELETE  /list-connections/:id : delete the "id" listConnection.
     *
     * @param id the id of the listConnection to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/list-connections/{id}")
    @Timed
    public ResponseEntity<Void> deleteListConnection(@PathVariable Long id) {
        log.debug("REST request to delete ListConnection : {}", id);

        listConnectionRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
